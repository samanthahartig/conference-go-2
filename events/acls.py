from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json
import logging
from pprint import pprint

logging.basicConfig(level=logging.DEBUG)


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": f"{city} {state}"}
    url = "https://api.pexels.com/v1/search/"
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content) #response is actual dictionary/object. content is one of the keys.
    pprint(content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_lat_long(city, state):
    base_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state}, USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(base_url, params=params)
    content = json.loads(response.content)
    logging.debug(
        content
    )  # Print to the log to figure out whats in the dictionary ...
    try:
        latitude = content[0].get("lat")
        longitude = content[0].get("lon")
        return {"latitude": latitude, "longitude": longitude}
    except(KeyError, IndexError):
        return None


def get_weather_data(city, state):
    lat_long = get_lat_long(city, state)
    if not lat_long:
        return None
    base_url = "http://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat_long.get("latitude"), #dictionary from line 46
        "lon": lat_long.get("longitude"),
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    response = requests.get(base_url, params=params)
    content = json.loads(response.content)
    try:
        temp = content["main"].get("temp")
        description = content["weather"][0].get("description") # .get is to avoid key error
        weather_data = {"temp": temp, "description": description}
    except(KeyError, IndexError):
        return None
    return weather_data


#def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary

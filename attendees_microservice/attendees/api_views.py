from django.http import JsonResponse
from .models import Attendee
from .encoders import AttendeesListEncoder, AttendeeDetailEncoder, ConferenceVO
from django.views.decorators.http import require_http_methods
import json


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            attendees, encoder=AttendeesListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)

        try:
            # THIS LINE IS ADDED
            conference_href = f"/api/conferences/{conference_vo_id}/"

            # THIS LINE CHANGES TO ConferenceVO and import_href
            conference = ConferenceVO.objects.get(import_href=conference_href)

            content["conference"] = conference

        # THIS CHANGES TO ConferenceVO
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


def api_show_attendee(request, id):
    attendee = Attendee.objects.get(id=id)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )
